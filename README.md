# Slider

A slider built with ES6, SCSS and Webpack 2.

[![Sauce Test Status](https://saucelabs.com/buildstatus/jsdevtom)](https://saucelabs.com/u/jsdevtom)

[![Sauce Test Status](https://saucelabs.com/browser-matrix/jsdevtom.svg)](https://saucelabs.com/u/jsdevtom)



### What is this repository for?

* To demonstrate how a slider (carousel) can be implemented
* Still in active development

### Commands

#### Install
`git clone https://jsdevtom@bitbucket.org/jsdevtom/slider.git && npm install`

or

`git clone https://jsdevtom@bitbucket.org/jsdevtom/slider.git && yarn install`

#### Develop

`npm run dev`

or

`yarn dev`

#### Test 


`npm run test`

or

`yarn test`