# Plan
To make a website the uses a modular slider library to display the slider in coherence with progressive enhancement. 

## Technologies

### CI/ Testing
 - SauceLabs for crossbrowser testing
 - Codeship/ BitBucket pipline for automated Deployment
 - BitBucket private
 - Karma + Jasmine for the tests

### FrontEnd
 - Plain JS
 - HTML
 - SCSS

### Build Tools
 - Webpack

## Stories
1. Make autoplay optional
2. Put people in control
3. Keep text short and clear
4. Don’t duplicate H1 tags
5. Touch friendly

## Steps
1. Set up basic build process, webpack, ES6, etc.
2. Register acounts