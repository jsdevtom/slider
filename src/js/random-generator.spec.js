import { randomInteger } from './random-generator'

describe('randomInteger', function () {
  it('should return number', function () {
    expect(typeof randomInteger()).toBe('number')
  })
})
