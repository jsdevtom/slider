import '../css/main.scss'
import { randomInteger } from './random-generator'

const sanityOutput = document.querySelector('#sanityOutput')

const outputRandomInt = () => {
  sanityOutput.textContent = randomInteger()
}

const buttonRndInt = document.querySelector('#randomInt')

buttonRndInt.addEventListener('click', outputRandomInt)
