// karma.conf.js  --  karma configuration

// if you import your existing 'webpack.config.js' setup here,
// be sure to read the note about 'entry' below.

// Bitbucket pipelines set this environment variable to true whenever a pipeline runs
const isInCI = process.env.CI

const customLaunchers = {
  sl_chrome: {
    base: 'SauceLabs',
    browserName: 'chrome',
    platform: 'Windows 7',
    version: '35'
  },
  sl_firefox: {
    base: 'SauceLabs',
    browserName: 'firefox',
    version: '30'
  },
  sl_ie_11: {
    base: 'SauceLabs',
    browserName: 'internet explorer',
    platform: 'Windows 8.1',
    version: '11'
  }
}

module.exports = function (config) {
  config.set({
    frameworks: ['jasmine'],

    files: [
      // all files ending in ".spec"
      'src/*.spec.js',
      'src/**/*.spec.js'
    // each file acts as entry point for the webpack configuration
    ],

    preprocessors: {
      // add webpack as preprocessor
      'src/*.spec.js': ['webpack'],
      'src/**/*.spec.js': ['webpack']
    },

    browsers: isInCI ? Object.keys(customLaunchers) : ['Chrome'],

    webpack: {
      // you don't need to specify the entry option because
      // karma watches the test entry points
      // webpack watches dependencies
    },

    webpackMiddleware: {
      noInfo: true,
      stats: {
        chunks: false
      }
    },

    plugins: [
      require('karma-webpack'),
      'karma-chrome-launcher',
      'karma-jasmine',
      'karma-sauce-launcher'
    ],

    singleRun: isInCI,

    sauceLabs: {
      build: require('./package.json').version,
      testName: 'Web App Tests'
    },
    customLaunchers: customLaunchers,
    reporters: isInCI ? ['dots', 'saucelabs'] : ['progress']
  })
}
